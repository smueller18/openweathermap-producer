FROM golang:1.12-alpine3.9 as builder

RUN apk add --no-cache git pkgconfig bash make g++ librdkafka-dev

WORKDIR /go/src/gitlab.com/smueller18/openweathermap-producer

ENV GO111MODULE on
COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go build -v -o openweathermap-producer producer.go


FROM alpine:3.9

RUN apk add --no-cache librdkafka

COPY --from=builder /go/src/gitlab.com/smueller18/openweathermap-producer/openweathermap-producer /usr/bin/openweathermap-producer

ENTRYPOINT ["/usr/bin/openweathermap-producer"]
